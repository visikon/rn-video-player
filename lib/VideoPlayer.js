"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const react_native_orientation_locker_1 = require("react-native-orientation-locker");
const react_native_video_1 = require("react-native-video");
const ControlPanel_1 = require("./ControlPanel");
// import { isTablet } from "../Utils";
const isTablet = () => false;
class VideoPlayer extends React.Component {
    constructor(props) {
        super(props);
        this._onOrientationDidChange = (orientation) => {
            if (this.state.systemRotationLock) { //This is to prevent rotation on Android if the system autolock is disabled on the device. Works out of the box on IOS.
                if (orientation === "PORTRAIT") {
                    this.leaveFullscreen();
                }
                if (orientation === 'LANDSCAPE-LEFT') {
                    this.enterFullscreen();
                }
                if (orientation === "LANDSCAPE-RIGHT") {
                    this.setState({ isFullscreen: true });
                    react_native_orientation_locker_1.default.lockToLandscapeRight();
                }
            }
        };
        this.state = {
            rate: 1,
            volume: 1,
            duration: 0.0,
            paused: false,
            repeat: false,
            currentTime: 0.0,
            seeking: false,
            // isFullscreen: this.props.alwaysFullScreen ? true : false,
            isFullscreen: false,
            orientation: isTablet() ? "LANDSCAPE" : "PORTRAIT",
            isBuffering: false,
            error: false,
            timeout: false,
            systemRotationLock: false
        };
        this.onProgress = this.onProgress.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
        this.onError = this.onError.bind(this);
        this.notifyListener = this.notifyListener.bind(this);
        this._onOrientationDidChange = this._onOrientationDidChange.bind(this);
        this.calculateTimeFromSeekerPosition = this.calculateTimeFromSeekerPosition.bind(this);
    }
    componentDidMount() {
        //Check to see if the system autolock is enabled or not.
        react_native_orientation_locker_1.default.getAutoRotateState((rotationLock) => this.setState({ systemRotationLock: rotationLock }));
        //Add the device orientation listener. And start the video in portrait mode
        if (this.props.addOrientationListener) {
            react_native_orientation_locker_1.default.addDeviceOrientationListener(this._onOrientationDidChange);
            react_native_orientation_locker_1.default.lockToPortrait();
        }
        this.notifyListener("start");
    }
    componentWillUnmount() {
        //Remove the listener when unmount, and set the screen to portrait
        if (this.props.addOrientationListener) {
            react_native_orientation_locker_1.default.removeDeviceOrientationListener(this._onOrientationDidChange);
            react_native_orientation_locker_1.default.lockToPortrait();
        }
        this.notifyListener("leave");
    }
    leaveFullscreen() {
        this.setState({ isFullscreen: false, });
        react_native_orientation_locker_1.default.lockToPortrait();
        react_native_orientation_locker_1.default.unlockAllOrientations();
    }
    enterFullscreen() {
        this.setState({ isFullscreen: true });
        react_native_orientation_locker_1.default.lockToLandscapeLeft();
    }
    notifyListener(event, currentTime, duration) {
        if (this.props.listener) {
            const time = currentTime === undefined ? this.state.currentTime : currentTime;
            const dur = duration === undefined ? this.state.duration : duration;
            this.props.listener(event, time, dur);
        }
    }
    /**
     * Set how much of the seekbar is filled and where the knob is placed on the bar
     * @param {float} position distance to leftside of seekbar.
     */
    onProgress(data) {
        if (!this.state.seeking) {
            this.setState({ currentTime: data.currentTime });
        }
    }
    /**
     * Return the time that the video should be at
     * based on where the seeker handle is.
     *
     * @return {float} time in ms based on seekerPosition.
     */
    calculateTimeFromSeekerPosition(percent, isMoving) {
        console.log("calculateTimeFromSeekerPosition");
        if (!isMoving) {
            console.log("percent", this.state.duration, percent);
            const currentTime = this.state.duration * percent;
            this.setState({ currentTime });
            console.log("seekerStoppedMoving", currentTime, this.state.duration, percent);
            this.player.seek(Math.floor(currentTime), 20);
            this.notifyListener("seekerChange", currentTime);
        }
    }
    onLoad(data) {
        console.log("onLoad", data.duration);
        let startPoint = 0.0;
        if (this.props.startPoint) {
            console.log("progress - there is progress", this.props.startPoint);
            startPoint = (data.duration * this.props.startPoint) / 100;
            this.player.seek(startPoint);
        }
        console.log("progress -  currentTime", startPoint);
        this.setState({ duration: data.duration, currentTime: startPoint });
    }
    onEnd() {
        if (this.props.onEnd) {
            this.setState({ currentTime: this.state.duration });
            this.props.onEnd(this.player);
        }
        else {
            console.log("onEnd() is undefined");
            this.setState({ paused: true });
        }
    }
    onError(data) {
        console.log("onError", data);
        this.notifyListener("error");
        this.setState({ error: true });
    }
    onBuffer(data) {
        console.log("onBuffer", data);
        this.setState({ isBuffering: data.isBuffering });
    }
    handlePlayButton() {
        if (Math.ceil(this.state.currentTime) >= Math.ceil(this.state.duration)) {
            this.player.seek(0);
            this.setState({ currentTime: 0.0 });
        }
        const paused = !this.state.paused;
        if (paused) {
            this.notifyListener("pausePressed");
        }
        else {
            this.notifyListener("playPressed");
        }
        this.setState({ paused });
    }
    pauseVideo() {
        this.setState({ paused: true });
    }
    toggleFullScreen(isFullscreen) {
        if (isFullscreen) {
            this.leaveFullscreen();
        }
        else {
            this.enterFullscreen();
        }
        this.notifyListener(isFullscreen ? "fullscreenExit" : "fullscreenEnter");
    }
    render() {
        const { x, y, width, height, screen_height, screen_width } = this.props.containerLayout;
        const absoluteBottom = screen_height - height - y;
        const absoluteRight = screen_width - width - x;
        const absoluteLeft = this.props.containerLayout.x;
        const absoluteTop = this.props.containerLayout.y;
        return (React.createElement(react_native_1.View, { style: this.state.isFullscreen || this.props.alwaysFullScreen ? styles.fullscreen_container : [styles.mobile_portrait_container, { left: absoluteLeft, top: absoluteTop, right: absoluteRight, bottom: absoluteBottom }] },
            React.createElement(react_native_video_1.default, { ref: (ref) => { this.player = ref; }, source: { uri: this.props.source, type: "mp4" }, repeat: this.state.repeat, paused: this.state.paused, rate: this.state.rate, volume: this.state.volume, resizeMode: "contain", onEnd: () => {
                    this.notifyListener("finished");
                    this.onEnd();
                }, style: styles.videoPlayerStyle, onProgress: this.onProgress, onLoad: this.onLoad, ignoreSilentSwitch: "ignore" }),
            React.createElement(ControlPanel_1.default, { onPlayButtonPress: () => this.handlePlayButton(), currentTime: this.state.currentTime, duration: this.state.duration, onSeekbarChange: this.calculateTimeFromSeekerPosition, paused: this.state.paused, onFullScreenChange: () => this.toggleFullScreen(this.state.isFullscreen), theme: this.props.theme, isBuffering: this.state.isBuffering, error: this.state.error, onBack: this.props.onBack, isFullscreen: this.state.isFullscreen, title: this.props.title, listener: this.notifyListener })));
    }
}
exports.default = VideoPlayer;
const styles = react_native_1.StyleSheet.create({
    mobile_portrait_container: {
        position: "absolute",
        top: 0, left: 0,
        // width: isTablet() ? "65%" : "100%",
        // aspectRatio: (16 / 9),
        backgroundColor: "black",
        elevation: 2,
        zIndex: 2,
    },
    fullscreen_container: {
        position: "absolute",
        top: 0, left: 0, right: 0, bottom: 0,
        backgroundColor: "black",
        zIndex: 2,
        elevation: 2,
        overflow: "visible",
    },
    videoPlayerStyle: {
        flex: 1,
    },
});
//# sourceMappingURL=VideoPlayer.js.map