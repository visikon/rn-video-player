"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const SeekerBar_1 = require("./SeekerBar");
const ControlIcon_1 = require("./ControlIcon");
const TIMEOUT_DELAY = 2700;
const CONTROL_PADDING = 12;
const PRIMARY_COLOR = "PRIMARY_COLOR";
const SECONDARY_COLOR = "SECONDARY_COLOR";
const TERTIARY_COLOR = "TERTIARY_COLOR";
class ControlPanel extends React.Component {
    constructor(props) {
        super(props);
        this.controls = {
            timeout_delay: 0,
            controlTimeout: -1,
        };
        this.prevShowControls = false;
        this.state = {
            showControls: false,
            controlFadeAnim: new react_native_1.Animated.Value(0),
            controlPanelWidth: 0,
        };
        this.controls.timeout_delay = TIMEOUT_DELAY;
        this.resetControlTimeout();
    }
    componentWillUnmount() {
        clearTimeout(this.controls.controlTimeout);
    }
    componentDidUpdate(prevProps) {
        if (prevProps.paused !== this.props.paused) {
            this.setState({ showControls: this.props.paused });
        }
    }
    /**
     * Set a timeout when the controls are shown
     * that hides them after a length of time.
     * Default is 2.7s
     */
    resetControlTimeout() {
        clearTimeout(this.controls.controlTimeout);
        this.controls.controlTimeout = setTimeout(() => {
            if (!this.props.paused && !this.props.isBuffering) {
                this.hideControls();
            }
        }, TIMEOUT_DELAY);
    }
    /**
     * Function to hide the controls. Sets our
     * state then calls the animation.
     */
    hideControls() {
        this.setState({ showControls: false });
        // this.hideControlAnimation();
    }
    wrapInTimeout(func) {
        return (...args) => {
            this.resetControlTimeout();
            func(...args);
        };
    }
    getColor(color, theme) {
        if (theme && theme[color]) {
            return theme[color];
        }
        switch (color) {
            case PRIMARY_COLOR:
                return "blue";
            case SECONDARY_COLOR:
                return "white";
            case TERTIARY_COLOR:
                return "gray";
            default:
                return "white";
        }
    }
    /**
   * Converts seconds to the format hours:minutes:seconds
   * @param {number} duration - number in seconds
   */
    secondsToTime(secs) {
        secs = Math.round(secs);
        let hours = Math.floor(secs / (60 * 60));
        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);
        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);
        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds < 10 ? `0${seconds}` : seconds,
        };
        if (obj.h <= 0) {
            return `${obj.m}:${obj.s}`;
        }
        else {
            return `${obj.h}:${obj.m}:${obj.s}`;
        }
    }
    getFont(theme) {
        if (this.props.theme && this.props.theme.FONT_FAMILY) {
            return this.props.theme.FONT_FAMILY;
        }
        return undefined;
    }
    renderBufferIcon(theme) {
        if (this.props.isBuffering) {
            return (React.createElement(react_native_1.View, { pointerEvents: "none", style: { position: "absolute", left: 0, right: 0, top: 0, bottom: 0, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" } }));
        }
        else {
            return;
        }
    }
    renderErrorIcon() {
        if (this.props.error) {
            return (React.createElement(VideoErrorMessage, null));
        }
        else {
            return;
        }
    }
    renderBottomControls(theme) {
        return (React.createElement(react_native_1.View, { style: { padding: CONTROL_PADDING, width: "100%", flexDirection: "row", alignItems: "center" } },
            React.createElement(react_native_1.Text, { style: { fontFamily: this.getFont(theme), marginLeft: 12, color: this.getColor(SECONDARY_COLOR, theme) } }, this.secondsToTime(this.props.currentTime < this.props.duration ? this.props.currentTime : this.props.duration)),
            React.createElement(SeekerBar_1.default, { duration: this.props.duration, currentTime: this.props.currentTime, onChange: this.wrapInTimeout(this.props.onSeekbarChange), theme: this.props.theme }),
            React.createElement(react_native_1.Text, { style: { fontFamily: this.getFont(theme), color: this.getColor(SECONDARY_COLOR, this.props.theme) } }, this.secondsToTime(this.props.duration)),
            React.createElement(ControlIcon_1.ControlIcon, { name: this.props.isFullscreen ? "exit_fullscreen" : "fullscreen", onPress: this.props.onFullScreenChange, size: 24, color: this.getColor(SECONDARY_COLOR), style: { marginRight: 12, marginLeft: 12 } })));
    }
    renderTopControls(theme) {
        return (React.createElement(react_native_1.View, { style: { width: "100%", flexDirection: "row", alignItems: "center", justifyContent: "space-between" } },
            React.createElement(react_native_1.View, { style: { flexDirection: "row", alignItems: "center" } },
                React.createElement(ControlIcon_1.ControlIcon, { name: "back", onPress: this.props.onBack, size: 24, color: this.getColor(SECONDARY_COLOR), style: { marginBottom: 12, marginTop: 12, marginLeft: 16, marginRight: 12 } }),
                React.createElement(react_native_1.Text, { style: { fontSize: 18, fontFamily: this.getFont(theme), color: this.getColor(SECONDARY_COLOR, theme) } }, this.props.title))));
    }
    renderMiddleControls(theme) {
        return (React.createElement(react_native_1.View, { style: { flex: 1, justifyContent: "center", alignItems: "center" } },
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => { this.hideControls(); console.log("hideControls press"); }, style: [styles.backdrop, { backgroundColor: "transparent" }] }),
            React.createElement(ControlIcon_1.ControlIcon, { name: this.props.currentTime < this.props.duration ? this.props.paused ? "play" : "pause" : "replay", onPress: this.wrapInTimeout(() => this.props.onPlayButtonPress()), size: 56, color: this.getColor(SECONDARY_COLOR), style: { marginRight: 12, marginLeft: 12 } })));
    }
    renderLoading() {
        if (this.props.duration === 0.0 && !this.props.error) {
            return (React.createElement(react_native_1.View, { style: [styles.backdrop] },
                React.createElement(react_native_1.View, { style: [styles.backdrop, { backgroundColor: "transparent", alignItems: "center", justifyContent: "center" }] },
                    React.createElement(react_native_1.ActivityIndicator, { size: "large", color: "white" })),
                this.renderTopControls(this.props.theme)));
        }
        else {
            return;
        }
    }
    renderControls(showControls) {
        if (this.prevShowControls !== showControls) {
            console.log("fade controls", showControls ? "in" : "out");
            react_native_1.Animated.timing(this.state.controlFadeAnim, {
                toValue: showControls ? 1 : 0,
                duration: 300,
            }).start();
        }
        this.prevShowControls = showControls;
        return (React.createElement(react_native_1.Animated.View, { style: [styles.backdrop, { opacity: this.state.controlFadeAnim }] },
            this.renderTopControls(this.props.theme),
            this.renderMiddleControls(this.props.theme),
            this.renderBottomControls(this.props.theme)));
    }
    render() {
        const { PRIMARY_COLOR, SECONDARY_COLOR, TERTIARY_COLOR, FONT_FAMILY } = this.props.theme;
        return (React.createElement(react_native_1.View, { style: styles.container, onLayout: event => {
                console.log("onLayout");
                this.setState({ controlPanelWidth: event.nativeEvent.layout.width });
            } },
            this.renderControls(this.state.showControls && this.props.duration > 0 && !this.props.error),
            this.renderLoading(),
            !this.state.showControls && !this.props.error && this.props.duration > 0 ?
                React.createElement(react_native_1.TouchableWithoutFeedback, { onPress: this.wrapInTimeout(() => { this.setState({ showControls: true }); }) },
                    React.createElement(react_native_1.View, { style: [styles.container, { backgroundColor: "transparent" }] },
                        React.createElement(react_native_1.View, { style: { position: "absolute", bottom: 0, left: 0, height: 3, backgroundColor: SECONDARY_COLOR, width: this.state.controlPanelWidth * (this.props.currentTime / this.props.duration) } })))
                : null));
    }
}
exports.default = ControlPanel;
// interface IconProps {
//     onPress?: () => void;
//     source: ImagePropertiesSourceOptions;
//     style?: any;
//     tintColor?: string;
// }
// function ControlIcon({ onPress, source, style, tintColor }: IconProps) {
//     return (
//         <TouchableOpacity onPress={onPress}>
//             {/* <Image style={[{ marginRight: 12, marginLeft: 12, height: 24, tintColor: tintColor ? tintColor : "#fff" }, style]} resizeMode="contain" source={source} /> */}
//         </TouchableOpacity>
//     );
// }
const VideoErrorMessage = () => {
    return (React.createElement(react_native_1.View, { style: [styles.container] },
        React.createElement(react_native_1.Text, { style: { fontSize: 24, color: "white" } }, "Der opstod en fejl"),
        React.createElement(react_native_1.Text, { style: { color: "white" } }, "Tjek din internetforbindelse")));
};
const styles = react_native_1.StyleSheet.create({
    container: {
        position: "absolute",
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        // backgroundColor: "rgba(0, 0, 0, 0.4)",
        justifyContent: "center",
        alignItems: "center",
    },
    backdrop: {
        position: "absolute",
        left: 0, top: 0, right: 0, bottom: 0,
        backgroundColor: "rgba(0,0,0,0.4)",
    }
});
//# sourceMappingURL=ControlPanel.js.map