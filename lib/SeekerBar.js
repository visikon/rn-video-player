"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const CIRCLE_HOTSPOT_SIZE = 60;
const CIRCLE_VISUAL_SIZE = 12;
const SEEKER_HOTSPOT_HEIGHT = 20;
const SEEKER_VISUAL_HEIGHT = 2;
const PRIMARY_COLOR = "PRIMARY_COLOR";
const SECONDARY_COLOR = "SECONDARY_COLOR";
const TERTIARY_COLOR = "TERTIARY_COLOR";
class SeekerBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seeking: false,
            previousKnobPosition: 0,
            knobPosition: 0,
            seekerWidth: 0,
            seekerHeight: SEEKER_HOTSPOT_HEIGHT,
            circleSize: CIRCLE_VISUAL_SIZE,
        };
        // Setup PanResponder
        this.panResponder = react_native_1.PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderGrant: this.handlePanResponderGrant.bind(this),
            onPanResponderMove: this.handlePanResponderMove.bind(this),
            onPanResponderRelease: this.handlePanResponderEnd.bind(this),
            onPanResponderTerminate: this.handlePanResponderEnd.bind(this),
        });
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (!this.state.seeking) {
            const percent = this.props.duration !== 0 ? nextProps.currentTime / this.props.duration : 0;
            this.setSeekerPosition(percent, this.state.seeking);
        }
    }
    handleStartShouldSetPanResponder(e, gestureState) {
        // Should we become active when the user presses down on the circle?
        return true;
    }
    handleMoveShouldSetPanResponder(e, gestureState) {
        // Should we become active when the user moves a touch over the circle?
        return true;
    }
    handlePanResponderGrant(e, gestureState) {
        this.setState({ seeking: true });
    }
    handlePanResponderMove(e, gestureState) {
        const percent = this.getProgressPercent(this.state.previousKnobPosition, gestureState.dx, this.state.seekerWidth); // this.constrainToSeekerMinMax(this.state.previousKnobPosition + (gestureState.dx / this.state.seekerWidth));
        this.setSeekerPosition(percent, this.state.seeking);
        this.props.onChange(this.constrainToSeekerMinMax(percent), true);
    }
    handlePanResponderEnd(e, gestureState) {
        const percent = this.getProgressPercent(this.state.previousKnobPosition, gestureState.dx, this.state.seekerWidth); // this.constrainToSeekerMinMax(this.state.previousKnobPosition + (gestureState.dx / this.state.seekerWidth));
        this.props.onChange(this.constrainToSeekerMinMax(percent), false);
        this.setState({
            seeking: false,
        });
        this.setSeekerPosition(percent, false);
    }
    calculateSeekerPosition(currentTime) {
        let duration = this.props.duration;
        const percent = currentTime / duration;
        return this.state.seekerWidth * percent;
    }
    getProgressPercent(previousPosition, dx, seekerWidth) {
        if (this.state.seekerWidth === 0) {
            return 0;
        }
        else {
            return previousPosition + dx / seekerWidth;
        }
    }
    /**
     * Contrain the location of the knob to be within the seeker
     *
     * @param {float} value position of seeker knob
     * @return {float} contrained position of seeker handle in px
     */
    constrainToSeekerMinMax(value = 0) {
        if (value <= 0) {
            return 0;
        }
        else if (value > 1) {
            return 1;
        }
        return value;
    }
    /**
     * Set how much of the seekbar is filled and where the knob is placed on the bar
     * @param {float} position distance to leftside of seekbar.
     */
    setSeekerPosition(percent = 0, seeking) {
        percent = this.constrainToSeekerMinMax(percent);
        this.setState({
            knobPosition: percent,
        });
        if (!seeking) {
            this.setState({ previousKnobPosition: percent });
        }
    }
    getColor(color, theme) {
        if (this.props.theme && theme[color]) {
            return theme[color];
        }
        switch (color) {
            case PRIMARY_COLOR:
                return "blue";
            case SECONDARY_COLOR:
                return "white";
            case TERTIARY_COLOR:
                return "gray";
            default:
                return "white";
        }
    }
    render() {
        return (React.createElement(react_native_1.View, { style: { flex: 1, marginLeft: 8, marginRight: 8 } },
            React.createElement(react_native_1.View, { style: { flex: 1, height: this.state.seekerHeight, backgroundColor: "transparent", justifyContent: "center" }, onLayout: event => {
                    this.setState({ seekerWidth: event.nativeEvent.layout.width - CIRCLE_VISUAL_SIZE });
                } },
                React.createElement(react_native_1.View, { style: { height: SEEKER_VISUAL_HEIGHT, backgroundColor: this.getColor(SECONDARY_COLOR, this.props.theme), borderRadius: SEEKER_VISUAL_HEIGHT / 2 } },
                    React.createElement(react_native_1.View, { style: [styles.progressFill, { backgroundColor: this.getColor(SECONDARY_COLOR, this.props.theme), width: this.state.knobPosition * this.state.seekerWidth + CIRCLE_VISUAL_SIZE / 2 }] }))),
            React.createElement(react_native_1.View, Object.assign({ style: [styles.circleHotspot, { left: this.state.knobPosition * this.state.seekerWidth - CIRCLE_HOTSPOT_SIZE / 2 + CIRCLE_VISUAL_SIZE / 2 }] }, this.panResponder.panHandlers),
                React.createElement(react_native_1.View, { style: [styles.circle, { backgroundColor: this.getColor(SECONDARY_COLOR, this.props.theme) }] }))));
    }
}
exports.default = SeekerBar;
const styles = react_native_1.StyleSheet.create({
    progressFill: {
        position: "absolute",
        height: SEEKER_VISUAL_HEIGHT,
        top: 0,
        borderRadius: SEEKER_VISUAL_HEIGHT / 2,
    },
    circle: {
        overflow: "visible",
        width: CIRCLE_VISUAL_SIZE,
        height: CIRCLE_VISUAL_SIZE,
        borderRadius: CIRCLE_VISUAL_SIZE / 2,
    },
    circleHotspot: {
        paddingTop: CIRCLE_VISUAL_SIZE / 4,
        width: CIRCLE_HOTSPOT_SIZE,
        height: CIRCLE_HOTSPOT_SIZE,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        backgroundColor: "transparent",
        top: SEEKER_HOTSPOT_HEIGHT / 2 - CIRCLE_HOTSPOT_SIZE / 2,
    },
});
//# sourceMappingURL=SeekerBar.js.map