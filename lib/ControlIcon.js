"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_vector_icons_1 = require("react-native-vector-icons");
const icoMoonConfig = require("../assets/selection.json");
const react_native_1 = require("react-native");
// try {
const Icon = react_native_vector_icons_1.createIconSetFromIcoMoon(icoMoonConfig);
// } catch (e) {
//     console.log("error creatingIconSet", e, icoMoonConfig);
// }
class ControlIcon extends React.Component {
    render() {
        const { name, color, size, style, onPress } = this.props;
        console.log("ControlIcon name", name);
        return (React.createElement(react_native_1.TouchableOpacity, { onPress: onPress },
            React.createElement(react_native_1.View, { style: style },
                React.createElement(Icon, { name: name, color: color, size: size }))));
    }
}
exports.ControlIcon = ControlIcon;
//# sourceMappingURL=ControlIcon.js.map